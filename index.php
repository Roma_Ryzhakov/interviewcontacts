<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<style>
    body{
        margin: 0;
        padding: 0;
        display: block;
    }
    .container{
        display: flex;
        flex-direction: column;
        align-items: center;
        max-width: 1170px;
        margin-left: auto;
        margin-right: auto;
        padding-left: 16px;
        padding-right: 16px;
    }
    .form{
        margin: 30px;
    }
    form input{
        min-width: 250px;
        min-height: 28px;
    }
    form button{
        height:35px;
    }
    .table {
        width: 100%;
        margin-bottom: 20px;
        border: 1px solid #dddddd;
        border-collapse: collapse;
        min-width: 600px;
    }
    .table th {
        width: 100%;
        font-weight: bold;
        padding: 5px;
        background: #efefef;
        border: 1px solid #dddddd;
    }
    .table td {
        width: 100%;
        border: 1px solid #dddddd;
        padding: 5px;
        min-width: 300px;
        text-align: center;
    }
    td:last-of-type{
        min-width: 70px;
        border: 1px solid rgb(160 160 160);
    }
</style>
<body>
<div class="container">
    <div class="div_form">
        <form action="Contacts/create.php" class="form" method="post">
            <input type="text" id="name" name="name" required placeholder="Введите имя">
            <input type="text" id="phone" name="phone" required placeholder="Введите номер телефона">
            <button type="submit">
                Создать пользователя
            </button>

        </form>
    </div>
    <div>
        <table class="table">
            <thead>
                <tr>
                    <th scope="col">ID</th>
                    <th scope="col">Имя</th>
                    <th scope="col">Номер телефона</th>
                </tr>
            </thead>
            <tbody>
            <?php
            $i = 1;
            $file = 'Contacts/contacts.json';

            if (file_exists($file)) {
            $json_data = file_get_contents($file);
            $contacts = json_decode($json_data, true);

                foreach($contacts as $key => $contact): ?>
                <tr>
                    <td><?php echo $key + 1; ?></td>
                    <td><?php echo $contact['name']; ?></td>
                    <td><?php echo $contact['phone']; ?></td>
                    <td>
                        <form method="post" action="Contacts/delete.php">
                            <input type="hidden" name="user_id" value="<?php echo $key; ?>">
                            <input type="submit" name="delete_user" value="Удалить">
                        </form>
                    </td>
                </tr>
            <?php
                endforeach;
            } else {
                echo "Файл с контактами не найден.";
            }
             ?>
            </tbody>
        </table>
    </div>
</div>

</body>
</html>
