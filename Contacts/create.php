<?php
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $name = $_POST['name'];
    $phone = $_POST['phone'];

    $file = 'contacts.json';

    $contacts = [];
    if(file_exists($file)){
        $json_data = file_get_contents($file);
        $contacts = json_decode($json_data, true);
    }

    $new_contact = array('name' => $name, 'phone' => $phone);
    $contacts[] = $new_contact;

    if (file_put_contents($file, json_encode($contacts, JSON_PRETTY_PRINT)) !== false) {
        echo "Контакт успешно добавлен в JSON файл.";
    } else {
        echo "Произошла ошибка при записи контакта в файл.";
    }

    // Перенаправление на другую страницу
    header("Location: ../index.php");
    exit();
}
?>