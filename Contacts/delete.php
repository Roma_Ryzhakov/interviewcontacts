<?php
if(isset($_POST['delete_user'])) {
    $id = $_POST['user_id'];

    $data = file_get_contents('contacts.json');
    $users = json_decode($data, true);

    unset($users[$id]);

    file_put_contents('contacts.json', json_encode($users, JSON_PRETTY_PRINT));

    header('Location: ../index.php');
    exit;
}
?>